package de.gsohs.bme5.podlike;

/**
 * Main Class that bootstraps the entire Application.
 */
public class Main {

    /**
     * Main class of this app that will be called to bootstrap the application.
     * It's implementation uses an AppBuilder and Factory instance to create the
     * Object graph and build the app structure
     *
     * @param args progamm arguments (e.g. java -jar podlike-full.jar argument1 argument2)
     */
    public static void main(String[] args) {
        AppBuilder.create().withFactory(HibernateFactory.getInstance()).build();
    }
}
