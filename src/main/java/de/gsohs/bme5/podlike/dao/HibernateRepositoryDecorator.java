package de.gsohs.bme5.podlike.dao;

import java.util.List;
import java.util.Map;

/**
 * Utility class that ensures a proper handling of Hibernate Sessions.
 * It decorates a so called "delegate Repository" and intercepts all method
 * calls defined in the {@link IRepository} interface by:
 * <ul>
 *     <li>retrieving of an EnityManager instance from {@link MyEmFactory}</li>
 *     <li>opening a transaction on methods that change data (save,delete)</li>
 *     <li>calling the method on the decorated delegate Repository</li>
 *     <li>closing the transaction after work was done.</li>
 *     <li>closing the Entity-Manager Session after work was done.</li>
 * </ul>
 * This Decorator is used in {@link de.gsohs.bme5.podlike.HibernateFactory#createRepository(String) HibernateFactory} factory methods
 * @see <a href="https://en.wikipedia.org/wiki/Decorator_pattern">Decorator Design Pattern</a>
 *
 * @param <T> the Model-Object-Type (JPA Entity) that the decorator handles
 * @param <S> the identifier-Type for the Entity
 */
public class HibernateRepositoryDecorator<T,S> implements IRepository<T,S> {

    private final AbstractRepository<T,S> decoratedRepo;

    public HibernateRepositoryDecorator(AbstractRepository<T, S> decoratedRepo) {
        this.decoratedRepo = decoratedRepo;
    }

    @Override
    public List<T> findAll() {
        decoratedRepo.entityManager = MyEmFactory.getInstance().getEntityManager();
        List<T> res = decoratedRepo.findAll();
        decoratedRepo.entityManager.close();
        return res;
    }

    @Override
    public List<T> findWithParams(Map<String, Object> params) {
        decoratedRepo.entityManager = MyEmFactory.getInstance().getEntityManager();
        List<T> res = decoratedRepo.findWithParams(params);
        decoratedRepo.entityManager.close();
        return res;
    }

    @Override
    public T findOne(S id) {
        decoratedRepo.entityManager = MyEmFactory.getInstance().getEntityManager();
        T res = decoratedRepo.findOne(id);
        decoratedRepo.entityManager.close();
        return res;
    }

    @Override
    public T save(T entity) {
        decoratedRepo.entityManager = MyEmFactory.getInstance().getEntityManager();
        decoratedRepo.entityManager.getTransaction().begin();
        decoratedRepo.save(entity);
        decoratedRepo.entityManager.getTransaction().commit();
        decoratedRepo.entityManager.close();
        return entity;
    }

    @Override
    public void delete(T entity) {
        decoratedRepo.entityManager = MyEmFactory.getInstance().getEntityManager();
        decoratedRepo.entityManager.getTransaction().begin();
        decoratedRepo.delete(entity);
        decoratedRepo.entityManager.getTransaction().commit();
        decoratedRepo.entityManager.close();
    }
}
