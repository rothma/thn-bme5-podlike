package de.gsohs.bme5.podlike.dao;

import java.util.List;
import java.util.Map;

/**
 * Interface that describes the protocol for a Data-Access-Object (DAO).
 * Common access methods are defined and can be implemented per concrete JPA-Model-Type
 *
 * @see <a href="https://www.tutorialspoint.com/design_pattern/data_access_object_pattern.htm">DAO Pattern</a>
 * @param <T> the Model-Object-Type (JPA Entity) that the subclass handles
 * @param <S> the identifier-Type for the Entity
 */
public interface IRepository<T,S> {
    /**
     * retrieves all Entity instances
     * @return a list of all model objects found in DB
     */
    List<T> findAll();

    /**
     * retrieves Entity instances that are compliant to the given filter spec
     * @param params a map (key=value) that filters the resulting list based on equality to given field values
     * @return a list of matching model objects.
     */
    List<T> findWithParams(Map<String,Object> params);

    /**
     * lookup of a single Entity based on its identified
     * @param id the identifier (marked with @Id)
     * @return the Enitity instance with matching primary key value
     */
    T findOne(S id);

    /**
     * synchronizes a detached or newly created entity with the Database.
     * @param entity the Entity that should be created or updated in DB
     * @return the MANAGED instance of the given entity (keep in mind that it is not the SAME Object instance)
     */
    T save(T entity);

    /**
     * removes the given Enitity from DB (triggers a DELETE operation when the HibernateSession is closed)
     * @param entity the entity instance that should be removed from DB
     */
    void delete(T entity);
}
