package de.gsohs.bme5.podlike.dao;

import jakarta.persistence.EntityManager;

/**
 * Base Class for a JPA Based Data-Access-Object. It just defines an @{@link EntityManager}
 * field that should be used by subclasses.
 *
 * @param <T> the Model-Object-Type (JPA Entity) that the subclass handles
 * @param <S> the identifier-Type for the Entity
 */
public abstract class AbstractRepository<T,S> implements IRepository<T,S> {
    protected EntityManager entityManager;


}
