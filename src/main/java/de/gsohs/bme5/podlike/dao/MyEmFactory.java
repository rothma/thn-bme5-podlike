package de.gsohs.bme5.podlike.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

/**
 * Class that setups and provides a {@link EntityManagerFactory} instance imported via
 * Hibernate OR-Mapper.
 * The usage of the Singleton design pattern ensures that a EntityManagerFactory is only
 * instantiated once in the application lifecycle.
 *
 * @see <a href="https://hibernate.org">Hibernate Webpage</a>
 */
public class MyEmFactory {
    private static MyEmFactory _instance;
    private final EntityManagerFactory emf;

    /**
     * private constructor only accessible by member method getInstance().
     * When called a EntityManagerFactory for the persistence unit "podlike"
     * is created. (see /src/main/resources/META-INF/persistence.xml for the
     * persistence unit configuration).
     */
    private MyEmFactory() {

        emf = Persistence.createEntityManagerFactory( "podlike" );
    }

    /**
     * getter for the instace of this class
     * @return the singleton instance of MyEmFactory
     */
    public static MyEmFactory getInstance() {
        if (_instance == null) {
            _instance = new MyEmFactory();
        }
        return _instance;
    }

    /**
     * getter for a JPA EntityManager (for usage in {@link IRepository} instances)
     * @return an EntityManager for Database Interactions
     */
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

}
