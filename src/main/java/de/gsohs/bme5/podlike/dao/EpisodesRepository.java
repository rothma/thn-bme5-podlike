package de.gsohs.bme5.podlike.dao;

import de.gsohs.bme5.podlike.model.Episode;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * JPA based {@link IRepository} implementation for {@link Episode} Entites. Uses
 * {@link jakarta.persistence.EntityManager} for interactions with a relational database
 */
public class EpisodesRepository extends AbstractRepository<Episode,Long> {
    @Override
    public List<Episode> findAll() {
        return entityManager.createQuery("from Episode",Episode.class).getResultList();
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List<Episode> findWithParams(Map<String, Object> params) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Episode> q = cb.createQuery(Episode.class);
        Root<Episode> p = q.from(Episode.class);
        List<Predicate> predicates = new ArrayList<>();
        params.forEach((k,v) -> {
            predicates.add(cb.equal(p.get(k), v));
        });
        q.select(p).where(predicates.toArray(new Predicate[0]));
        TypedQuery<Episode> query = entityManager.createQuery(q);
        return query.getResultList();
    }

    @Override
    public Episode findOne(Long id) {
        return entityManager.find(Episode.class,id);
    }

    @Override
    public Episode save(Episode entity) {
        return entityManager.merge(entity);
    }

    @Override
    public void delete(Episode entity) {
        entityManager.remove(entity);
    }
}
