package de.gsohs.bme5.podlike.dao;

import de.gsohs.bme5.podlike.model.User;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * JPA based {@link IRepository} implementation for {@link User} Entites. Uses
 * {@link jakarta.persistence.EntityManager} for interactions with a relational database
 */
public class UserRepository extends AbstractRepository<User, Long> {
    @Override
    public List<User> findAll() {
        return entityManager.createQuery("from User u", User.class).getResultList();
    }

    @Override
    public List<User> findWithParams(Map<String, Object> params) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> q = cb.createQuery(User.class);
        Root<User> p = q.from(User.class);
        List<Predicate> predicates = new ArrayList<>();
        params.forEach((k,v) -> {
            predicates.add(cb.equal(p.get(k), v));
        });
        q.select(p).where(predicates.toArray(new Predicate[0]));
        TypedQuery<User> query = entityManager.createQuery(q);
        return query.getResultList();
    }

    @Override
    public User findOne(Long id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public User save(User entity) {
        return entityManager.merge(entity);
    }

    @Override
    public void delete(User entity) {
        entityManager.remove(entity);
    }
}
