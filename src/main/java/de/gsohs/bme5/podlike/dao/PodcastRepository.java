package de.gsohs.bme5.podlike.dao;

import de.gsohs.bme5.podlike.model.Podcast;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * JPA based {@link IRepository} implementation for {@link Podcast} Entites. Uses
 * {@link jakarta.persistence.EntityManager} for interactions with a relational database
 */
public class PodcastRepository extends AbstractRepository<Podcast,Long> {

    @Override
    public List<Podcast> findAll() {
        List<Podcast> res = entityManager.createQuery("from Podcast",Podcast.class).getResultList();
        return res;
    }

    @Override
    public List<Podcast> findWithParams(Map<String, Object> params) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Podcast> q = cb.createQuery(Podcast.class);
        Root<Podcast> p = q.from(Podcast.class);
        List<Predicate> predicates = new ArrayList<>();
        params.forEach((k,v) -> {
            predicates.add(cb.equal(p.get(k), v));
        });
        q.select(p).where(predicates.toArray(new Predicate[0]));
        TypedQuery<Podcast> query = entityManager.createQuery(q);
        return query.getResultList();
    }

    @Override
    public Podcast findOne(Long id) {
        return entityManager.find(Podcast.class,id);
    }

    @Override
    public Podcast save(Podcast entity) {
        return entityManager.merge(entity);
    }

    @Override
    public void delete(Podcast entity) {
        entityManager.remove(entity);
    }
}
