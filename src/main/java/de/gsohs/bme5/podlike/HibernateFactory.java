package de.gsohs.bme5.podlike;

import de.gsohs.bme5.podlike.controller.*;
import de.gsohs.bme5.podlike.dao.*;
import de.gsohs.bme5.podlike.model.Podcast;
import de.gsohs.bme5.podlike.model.User;
import de.gsohs.bme5.podlike.util.EpisodeConverter;
import de.gsohs.bme5.podlike.util.PodcastConverter;

/**
 * Implemetation of {@link IFactory} that creates Controllers and associated
 * Repositories that interact with a relational database with the help of Hibernate
 * as object-relational mapper
 */
public class HibernateFactory implements IFactory {

    private static HibernateFactory _instance;

    /**
     * singleton instance getter
     * @return a HibernateFactory instance
     */
    public static HibernateFactory getInstance() {
        if(_instance == null) {
            _instance = new HibernateFactory();
        }
        return _instance;
    }

    @Override
    public IRepository createRepository(String name) {
        if (name.equals(PodcastRepository.class.getSimpleName())) {
            return new HibernateRepositoryDecorator(new PodcastRepository());
        }
        if (name.equals(EpisodesRepository.class.getSimpleName())) {
            return new HibernateRepositoryDecorator(new EpisodesRepository());
        }
        if(name.equals(UserRepository.class.getSimpleName())) {
            return new HibernateRepositoryDecorator(new UserRepository());
        }
        return null;
    }

    @Override
    public IController createController(String name, IRepository... repositories) {
        if (name.equals(PodcastController.class.getSimpleName())
            && repositories.length == 2) {
            return new PodcastController((IRepository<Podcast,Long>) repositories[0],repositories[1]);
        }
        if(name.equals(MediaController.class.getSimpleName()) && repositories.length == 2) {
            return new MediaController(repositories[0],repositories[1]);
        }
        if (name.equals(SessionHandler.class.getSimpleName()) && repositories.length == 1) {
            return new SessionHandler((IRepository<User,Long>) repositories[0]);
        }
        if(name.equals(PodcastRestController.class.getSimpleName()) && repositories.length == 1) {
            return new PodcastRestController(repositories[0], new PodcastConverter());
        }
        if(name.equals(EpisodeRestController.class.getSimpleName()) && repositories.length == 1) {
            return new EpisodeRestController(repositories[0],new EpisodeConverter());
        }
        return null;
    }
}
