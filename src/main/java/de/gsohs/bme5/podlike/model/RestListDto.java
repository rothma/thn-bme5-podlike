package de.gsohs.bme5.podlike.model;

import java.util.List;
import java.util.Map;

public class RestListDto {
    private List<RestEntityDto> embed;
    public Map<String,String> getMeta() {
        return Map.of("totalCount",Integer.toString(embed.size()));
    }

    public List<RestEntityDto> getEmbed() {
        return embed;
    }

    public void setEmbed(List<RestEntityDto> embed) {
        this.embed = embed;
    }
}
