package de.gsohs.bme5.podlike.model;

import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.annotations.Type;

import jakarta.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.Types;
import java.util.List;

@Entity
public class Podcast implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "podcast_gen")
    @SequenceGenerator(name = "podcast_gen" ,sequenceName = "podcast_seq", allocationSize = 1)
    private Long id;

    @Column(length = 120, nullable = false)
    private String title;

    @Column(length = 1000)
    private String description;

    @Column(length = 120)
    private String author;

    @Column(length = 255)
    private String link;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "category_podcast",
            joinColumns = {@JoinColumn(name = "category_id")},
            inverseJoinColumns = {@JoinColumn(name = "podcast_id")}
    )
    private List<Category> categories;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean explicit = false;

    @Lob
    @JdbcTypeCode(Types.VARBINARY)
    private Blob image;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Boolean getExplicit() {
        return explicit;
    }

    public void setExplicit(Boolean explicit) {
        this.explicit = explicit;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }
}
