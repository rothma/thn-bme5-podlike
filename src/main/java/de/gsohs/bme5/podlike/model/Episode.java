package de.gsohs.bme5.podlike.model;

import jakarta.persistence.*;
import org.hibernate.annotations.JdbcTypeCode;

import java.sql.Blob;
import java.sql.Types;
import java.util.Date;

@Entity
public class Episode {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "episode_gen")
    @SequenceGenerator(name = "episode_gen" ,sequenceName = "episode_seq", allocationSize = 1)
    private Long id;

    @Column(length = 120, nullable = false)
    private String title;

    @ManyToOne
    @JoinColumn(name = "podcast_id", nullable = false)
    private Podcast podcast;

    @Temporal(TemporalType.DATE)
    private Date publicationDate;

    @Column(length = 3000)
    private String description;

    @Column(length = 120)
    private String mimeType;

    @Lob
    @JdbcTypeCode(Types.VARBINARY)
    private Blob media;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Podcast getPodcast() {
        return podcast;
    }

    public void setPodcast(Podcast podcast) {
        this.podcast = podcast;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Blob getMedia() {
        return media;
    }

    public void setMedia(Blob media) {
        this.media = media;
    }
}
