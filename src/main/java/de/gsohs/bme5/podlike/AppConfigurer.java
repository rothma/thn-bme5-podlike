package de.gsohs.bme5.podlike;

import de.gsohs.bme5.podlike.controller.*;
import de.gsohs.bme5.podlike.exception.ConversionException;
import de.gsohs.bme5.podlike.exception.EntityConsistencyException;
import de.gsohs.bme5.podlike.exception.ResourceNotFoundException;
import io.javalin.Javalin;
import io.javalin.apibuilder.CrudHandler;
import io.javalin.core.security.AccessManager;

import static io.javalin.apibuilder.ApiBuilder.crud;

/**
 * Responsible for configuring a Javalin instance.
 * <ul>
 *     <li>common configurations</li>
 *     <li>exception, mappings</li>
 *     <li>access-control</li>
 *     <li>defining endpoints (map HTTP methods to paths and Handler-Implementations)</li>
 * </ul>
 */
public class AppConfigurer {
    private final AppBuilder appBuilder;

    /**
     * Constructor.
     * @param appBuilder an AppBuilder instance (in order to access javalin and App-Controllers)
     */
    public AppConfigurer(AppBuilder appBuilder) {
        this.appBuilder = appBuilder;
    }

    /**
     * uses Javalin instance via {@link AppBuilder} and configures it.
     */
    public void configure() {
        Javalin javalin = this.appBuilder.getJavalin();
        javalin.exception(ResourceNotFoundException.class, (e,ctx) -> {
           ctx.status(404);
        }).error(404, ctx -> {
            ctx.result("Resource not found");
        });
        javalin.exception(EntityConsistencyException.class, (e, ctx) -> {
            ctx.status(404);
        }).error(404, ctx -> {
            ctx.result("Resource not found");
        });
        javalin.exception(ConversionException.class, (e,ctx) -> {
            ctx.status(400);
        }).error(400, ctx -> {
            ctx.result("check your request. path params or payload is not correct.");
        });
        javalin.get("/", ctx -> ctx.redirect("/podcasts"));
        javalin.get("/login", ctx-> ctx.render("login.html"));
        javalin.post("/login", ((SessionHandler)this.appBuilder.getBean(SessionHandler.class.getSimpleName())).handleLogin);
        javalin.get("/podcasts",((PodcastController)this.appBuilder.getBean(PodcastController.class.getSimpleName())).getAllPodcasts);
        javalin.get("/podcasts/add",((PodcastController)this.appBuilder.getBean(PodcastController.class.getSimpleName())).getEditPodcast);
        javalin.get("/podcasts/{id}/edit",((PodcastController)this.appBuilder.getBean(PodcastController.class.getSimpleName())).getEditPodcast);
        javalin.get("/podcasts/{id}/image",((MediaController)this.appBuilder.getBean(MediaController.class.getSimpleName())).getImage);
        javalin.post("/podcasts/save",((PodcastController)this.appBuilder.getBean(PodcastController.class.getSimpleName())).postEditPodcast);
        javalin.get("/podcasts/{id}",((PodcastController)this.appBuilder.getBean(PodcastController.class.getSimpleName())).getPodcast);
        javalin.get("/podcasts/{id}/episodes/{episodeId}/media",((MediaController)this.appBuilder.getBean(MediaController.class.getSimpleName())).getAudio);
        javalin.get("/podcasts/{id}/episodes/add",((PodcastController)this.appBuilder.getBean(PodcastController.class.getSimpleName())).getEditEpisode);
        javalin.get("/podcasts/{id}/episodes/{episodeId}/edit",((PodcastController)this.appBuilder.getBean(PodcastController.class.getSimpleName())).getEditEpisode);
        javalin.post("/podcasts/{id}/episodes/save",((PodcastController)this.appBuilder.getBean(PodcastController.class.getSimpleName())).postEditEpisode);
        javalin.routes(() -> {
           crud("/api/podcasts/{id}",(CrudHandler) this.appBuilder.getBean(PodcastRestController.class.getSimpleName()));
        });
        javalin.routes(() -> {
            crud("/api/podcasts/{podcastId}/episodes/{id}", (CrudHandler) this.appBuilder.getBean(EpisodeRestController.class.getSimpleName()));
        });
    }
}
