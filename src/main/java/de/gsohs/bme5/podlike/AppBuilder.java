package de.gsohs.bme5.podlike;

import de.gsohs.bme5.podlike.controller.*;
import de.gsohs.bme5.podlike.dao.*;
import io.javalin.Javalin;
import io.javalin.core.security.AccessManager;
import io.javalin.http.staticfiles.Location;

import java.util.HashMap;

/**
 * This class acts as Builder and uses an {@link IFactory Factory} instance to coordinate the
 * creation an foremost building object associations. In order to provide access to business objects
 * it holds the references to app object instances in a map that can be accessed via {@link AppBuilder#getBean(String)}.
 * The builder can be called as singleton and used with 'method chaining' approach.
 *
 * @see <a href="https://en.wikipedia.org/wiki/Builder_pattern">Builder Pattern</a>
 * @see <a href="https://en.wikipedia.org/wiki/Method_chaining">Method Chaining</a>
 */
public class AppBuilder {
    private static AppBuilder _instance;
    private IFactory factory;
    private Javalin javalin;
    private AppConfigurer appConfigurer;
    private final HashMap<String,Object> registry = new HashMap<>();

    /**
     * creates an AppBuilder instance or returns an already created instance (singleton pattern).
     * @return an instancce of this builder
     */
    public static AppBuilder create() {
        if (_instance == null) {
            _instance = new AppBuilder();
        }
        _instance.appConfigurer = new AppConfigurer(_instance);
        return _instance;
    }

    /**
     * sets the {@link IFactory} instance to be used with this AppBuilder. This way the actual Controller
     * or Repository Instances are adaptable (different implementations or Objects for Unit-Tests etc).
     * @param factory a factory instance used to construct Controller and Repository instances
     * @return the AppBuilder itself
     */
    public AppBuilder withFactory(IFactory factory) {
        this.factory = factory;
        return this;
    }

    /**
     * getter for the javalin instance this Builder has configured (may return null if called before build() method)
     * @return the javalin instance of this builder
     */
    public Javalin getJavalin() { return this.javalin;}

    /**
     * this method is responsible for constructing the object graph of business objects for the application.
     * It is responsible for creating instances with the help of the given {@link IFactory} instance in the appropriate
     * order and associating them accordingly and creating a javalin instance which is configured with the help of an
     * AppConfigurer.
     *
     * @return the AppBuilder itself.
     */
    public AppBuilder build() {
        this.registry.put(MyEmFactory.class.getSimpleName(),MyEmFactory.getInstance());
        this.registry.put(PodcastRepository.class.getSimpleName(),factory.createRepository(PodcastRepository.class.getSimpleName()));
        this.registry.put(EpisodesRepository.class.getSimpleName(),factory.createRepository(EpisodesRepository.class.getSimpleName()));
        this.registry.put(UserRepository.class.getSimpleName(),factory.createRepository(UserRepository.class.getSimpleName()));
        this.registry.put(
                PodcastController.class.getSimpleName(),
                factory.createController(
                        PodcastController.class.getSimpleName(),
                        (IRepository) this.registry.get(PodcastRepository.class.getSimpleName()),
                        (IRepository) this.registry.get(EpisodesRepository.class.getSimpleName())
                )
        );
        this.registry.put(
                MediaController.class.getSimpleName(),
                factory.createController(
                        MediaController.class.getSimpleName(),
                        (IRepository) this.registry.get(PodcastRepository.class.getSimpleName()),
                        (IRepository) this.registry.get(EpisodesRepository.class.getSimpleName())
                )
        );
        this.registry.put(
                SessionHandler.class.getSimpleName(),
                factory.createController(
                        SessionHandler.class.getSimpleName(),
                        (IRepository) this.registry.get(UserRepository.class.getSimpleName())
                )
        );
        this.registry.put(
                PodcastRestController.class.getSimpleName(),
                factory.createController(
                        PodcastRestController.class.getSimpleName(),
                        (IRepository) this.registry.get(PodcastRepository.class.getSimpleName())
                )
        );
        this.registry.put(
                EpisodeRestController.class.getSimpleName(),
                factory.createController(
                        EpisodeRestController.class.getSimpleName(),
                        (IRepository) this.registry.get(EpisodesRepository.class.getSimpleName())
                )
        );
        this.javalin = Javalin.create(conf -> {
            conf.addStaticFiles("/static", Location.CLASSPATH);
            conf.accessManager((AccessManager) this.getBean(SessionHandler.class.getSimpleName()));
            conf.enableCorsForAllOrigins();
        }).start(8080);
        if(this.appConfigurer != null) {
            this.appConfigurer.configure();
        }
        return this;
    }

    /**
     * getter for business objects.
     * @param name The key is formatted as 'Class.class.getSimpleName()'
     * @return the Business object created by this builder (class cast necessary)
     */
    public Object getBean(String name) {
        return this.registry.get(name);
    }
}
