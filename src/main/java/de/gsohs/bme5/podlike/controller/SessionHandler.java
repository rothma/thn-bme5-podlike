package de.gsohs.bme5.podlike.controller;

import de.gsohs.bme5.podlike.dao.IRepository;
import de.gsohs.bme5.podlike.model.User;
import io.javalin.core.security.AccessManager;
import io.javalin.core.security.RouteRole;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * special Controller that handles form based login and implements javalin's {@link AccessManager}
 */
public class SessionHandler implements AccessManager, IController {

    private IRepository<User,Long> userRepo;

    /**
     * Constructor
     * @param repository a {@link de.gsohs.bme5.podlike.dao.UserRepository} instance used for looking up application users
     */
    public SessionHandler(IRepository<User,Long> repository) {
        this.userRepo = repository;
    }

    /**
     * implements the {@link AccessManager} interface. Javalin calls this method before
     * every {@link Handler} is invoked. By checking the existence of a 'currentuser' session
     * attribute access can be granted (by calling handler.handle(context)) or denied by
     * throwing an exception or (in our case) redirecting the User-Agent to the login page
     *
     * @param handler the {@link Handler} that is elected by javalin as successor of this method
     * @param context the {@link Context} object of the actual HTTP request we examine
     * @param roleSet unused in this app. (needed to lookup an application role for role based access)
     * @throws Exception thrown when access should be denied with an Exception (optional and not used)
     */
    @Override
    public void manage(@NotNull Handler handler, @NotNull Context context, @NotNull Set<RouteRole> roleSet) throws Exception {
        if (context.path().endsWith("edit")
                || context.path().endsWith("save")
                || context.path().endsWith("add")) {
            Long currentUser = context.sessionAttribute("currentuser");
            if (currentUser == null) {
                context.status(401).redirect("/login");
            } else {
                handler.handle(context);
            }
        } else {
            handler.handle(context);
        }
    }

    /**
     * handling a login Form-Post. Lookups up the user by username from DB and compares
     * the hashed password for equality. Upon successful login, the Web-Session is populated
     * with an "currentuser" attribute, otherwise the user will be redirected to the login page
     * again.
     */
    public Handler handleLogin = context -> {
        LoggerFactory.getLogger(SessionHandler.class).info(hashPassword(context.formParam("password")));
        if (context.formParam("username") == null
                || Objects.requireNonNull(context.formParam("username")).isEmpty()
                || context.formParam("password") == null
                || Objects.requireNonNull(context.formParam("password")).isEmpty()) {
            context.render("/login.html");
        } else {
            try {
                List<User> user = userRepo.findWithParams(Map.of("username",context.formParam("username")));
                if (user != null && user.size() == 1
                        && user.get(0).getPassword() != null
                        && user.get(0).getPassword().equals(hashPassword(context.formParam("password")))
                        && user.get(0).getUsername() != null
                        && user.get(0).getUsername().equals(context.formParam("username"))) {
                    context.sessionAttribute("currentuser", user.get(0).getId());
                    context.redirect("/");
                } else {
                    context.status(401).html("Access Denied");
                }
            } catch (Throwable ex) {
                context.status(501).html("Internal Error " + ex.getMessage());
            }
        }
    };

    private String hashPassword(String originalPass) {
        return DigestUtils.sha256Hex(originalPass);
    }
}
