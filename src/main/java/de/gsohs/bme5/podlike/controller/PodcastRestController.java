package de.gsohs.bme5.podlike.controller;

import de.gsohs.bme5.podlike.dao.IRepository;
import de.gsohs.bme5.podlike.exception.ConversionException;
import de.gsohs.bme5.podlike.model.Podcast;
import de.gsohs.bme5.podlike.model.RestEntityDto;
import de.gsohs.bme5.podlike.model.RestListDto;
import de.gsohs.bme5.podlike.util.PodcastConverter;
import io.javalin.apibuilder.CrudHandler;
import io.javalin.http.Context;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller that handles RESTful HTTP calls (with JSON representation type). It implements
 * Javalin's {@link CrudHandler} interface to be compliant with common Create-Read-Update-Delete paradigm.
 * it uses a {@link PodcastConverter} in order to convert JPA-Entity objects to Data-Transfer-Objects
 */
public class PodcastRestController implements IController, CrudHandler {

    private IRepository<Podcast,Long> podcastRepository;
    private PodcastConverter converter;

    /**
     * Constructor.
     * @param podcastRepository a {@link de.gsohs.bme5.podlike.dao.PodcastRepository} instance
     * @param converter a {@link PodcastConverter} instance
     */
    public PodcastRestController(IRepository<Podcast,Long> podcastRepository, PodcastConverter converter) {
        this.podcastRepository = podcastRepository;
        this.converter = converter;
    }

    /**
     * creates a new Podcast Resource via HTTP POST to the path '/api/podcasts'.
     * Successful creation is confirmed by HTTP Status 201 (created) and Location Header.
     *
     * @param ctx a {@link Context} instance to read request information and give back a response
     */
    @Override
    public void create(@NotNull Context ctx) {
        RestEntityDto body = ctx.bodyAsClass(RestEntityDto.class);
        Podcast pod = null;
        try {
            pod = this.converter.reconvert(body, null);
            pod = this.podcastRepository.save(pod);
            ctx.header("Location","/api/podcasts/"+pod.getId()).status(201);
        } catch (ConversionException e) {
            ctx.status(501);
        }
    }

    /**
     * deletes a Podcast Resource via HTTP DELETE to the path '/api/podcasts/:id'
     *
     * @param context a {@link Context} instance to read request information and give back a response
     * @param s the path param value for the resource identifier
     */
    @Override
    public void delete(@NotNull Context context, @NotNull String s) {
        this.podcastRepository.delete(this.podcastRepository.findOne(Long.parseLong(s)));
    }

    /**
     * retrieves a list of Podcasts via HTTP GET to the path '/api/podcasts'
     *
     * @param ctx a {@link Context} instance to read request information and give back a response
     */
    @Override
    public void getAll(@NotNull Context ctx) {
        List<RestEntityDto> resL = new ArrayList<>();
        for(Podcast p : this.podcastRepository.findAll()) {
            resL.add(this.converter.convert(p));
        }
        RestListDto res = new RestListDto();
        res.setEmbed(resL);
        ctx.json(res);
    }

    /**
     * retrieves a Podcast Resource via HTTP GET to the path '/api/podcasts/:id'
     *
     * @param ctx a {@link Context} instance to read request information and give back a response
     * @param s the path param value for the resource identifier
     */
    @Override
    public void getOne(@NotNull Context ctx, @NotNull String s) {
        Podcast p = this.podcastRepository.findOne(Long.parseLong(s));
        if(p == null) {
            ctx.status(404);
        } else {
            ctx.json(this.converter.convert(p));
        }
    }

    /**
     * updates a Podcast Resource via HTTP PATCH to the path '/api/podcasts/:id'
     *
     * @param ctx a {@link Context} instance to read request information and give back a response
     * @param s the path param value for the resource identifier
     */
    @Override
    public void update(@NotNull Context ctx, @NotNull String s) {
        RestEntityDto body = ctx.bodyAsClass(RestEntityDto.class);
        Podcast pod = this.podcastRepository.findOne(Long.parseLong(s));
        try {
            pod = this.converter.reconvert(body, pod);
            pod.setId(Long.parseLong(s));
            pod = this.podcastRepository.save(pod);
            ctx.json(pod);
        } catch (ConversionException e) {
            ctx.status(501);
        }
    }
}
