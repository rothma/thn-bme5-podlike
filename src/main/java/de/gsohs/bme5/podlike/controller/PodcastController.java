package de.gsohs.bme5.podlike.controller;

import de.gsohs.bme5.podlike.dao.IRepository;
import de.gsohs.bme5.podlike.exception.ResourceNotFoundException;
import de.gsohs.bme5.podlike.model.Episode;
import de.gsohs.bme5.podlike.model.Podcast;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.hibernate.engine.jdbc.BlobProxy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller that handles all requests to the application where server-side view processing
 * with the help of Thymeleaf-Templates (View) and Hibernate Repositories (Model) is done.
 * Note: Handlers are implemented as functional interface implementation in appropriate object
 * fields via lambda expression
 */
public class PodcastController implements IController {
    private IRepository<Podcast,Long> repository;
    private IRepository<Episode,Long> repoEpisode;

    /**
     * Constructor for PodcastController
     * @param repository a {@link de.gsohs.bme5.podlike.dao.PodcastRepository} instance
     * @param repoEpisode a {@link de.gsohs.bme5.podlike.dao.EpisodesRepository} instance
     */
    public PodcastController(IRepository<Podcast,Long> repository, IRepository<Episode,Long> repoEpisode) {
        this.repository = repository;
        this.repoEpisode = repoEpisode;
    }

    /**
     * handles a HTTP get for display of all podcasts found in DB
     */
    public Handler getAllPodcasts = (ctx) -> ctx.render(
            "get_podcasts.html",
            Map.of("podcasts",this.repository.findAll())
    );

    /**
     * handles a HTTP get for display of a specific podcast
     */
    public Handler getPodcast = ctx -> {
        Podcast podcast = createOrGetPodcast(ctx,true);
        List<Episode> episodes = this.repoEpisode.findWithParams(Map.of("podcast",podcast));
        ctx.render("get_podcast.html",Map.of("podcast",podcast,"episodes",episodes));
    };

    /**
     * handles a HTTP get for display of a podcast editing form
     */
    public Handler getEditPodcast = ctx -> {
        Podcast podcast = createOrGetPodcast(ctx,false);
        ctx.render("get_edit_podcast.html",Map.of("podcast",podcast,"errors",new HashMap<String, List<String>>()));
    };

    /**
     * handles a HTTP form-post for creation or update of a podcast
     */
    public Handler postEditPodcast = ctx -> {
        Podcast podcast = new Podcast();
        podcast.setId(ctx.formParamAsClass("id",Long.class).get());
        podcast.setDescription(ctx.formParam("description"));
        podcast.setAuthor(ctx.formParam("author"));
        podcast.setLink(ctx.formParam("link"));
        if(ctx.formParamAsClass("explicit",String.class).getOrDefault("off").equals("on")) {
            podcast.setExplicit(true);
        } else {
            podcast.setExplicit(false);
        }
        boolean hasErrors = false;
        Map<String,String> errorMap = new HashMap<>();
        if(ctx.formParam("title").trim().equals("")) {
            hasErrors = true;
            errorMap.put("title","Titel ist ein Pflichtfeld");
        }
        if(!ctx.uploadedFile("image").getContentType().equals("image/jpeg")) {
            hasErrors = true;
            errorMap.put("image","Upload muss ein jpeg sein");
        }
        if(hasErrors) {
            ctx.render("get_edit_podcast.html",Map.of("podcast",podcast,"errors",errorMap));
        } else {
            podcast.setImage(BlobProxy.generateProxy(ctx.uploadedFile("image").getContent().readAllBytes()));
            podcast.setTitle(ctx.formParam("title"));
            repository.save(podcast);
            ctx.redirect("/podcasts");
        }
    };

    /**
     * handles a HTTP get for display of a episode editing form
     */
    public Handler getEditEpisode = ctx -> {
        Podcast podcast = createOrGetPodcast(ctx,true);
        Episode episode = createOrGetEpisode(ctx,false);
        episode.setPodcast(podcast);
        ctx.render("get_edit_episode.html",Map.of("podcast",podcast,"episode",episode,"errors",new HashMap<String,String>()));
    };

    /**
     * handles a HTTP form-post for creation or update of a podcast
     */
    public Handler postEditEpisode = ctx -> {
        Podcast podcast = createOrGetPodcast(ctx, true);
        Episode episode = createOrGetEpisode(ctx, false);
        episode.setPodcast(podcast);
        episode.setTitle(ctx.formParam("title"));
        episode.setDescription(ctx.formParam("description"));
        boolean hasErrors = false;
        Map<String,String> errorMap = new HashMap<>();
        if(ctx.uploadedFile("media") == null ||
                !ctx.uploadedFile("media").getContentType().equals("audio/mpeg")) {
            hasErrors = true;
            errorMap.put("media","File must be a mp3");
        }
        if (hasErrors) {
            ctx.render("get_edit_episode.html",Map.of("podcast",podcast,"episode",episode,"errors",errorMap));
        } else {
            episode.setMedia(BlobProxy.generateProxy(ctx.uploadedFile("media").getContent().readAllBytes()));
            episode.setMimeType("audio/mpeg");
            repoEpisode.save(episode);
            ctx.redirect("/podcasts/" + podcast.getId().toString());
        }
    };

    Podcast createOrGetPodcast(Context ctx, Boolean failNotFound) throws ResourceNotFoundException {
        if(!ctx.pathParamMap().containsKey("id") && failNotFound) {
            throw new ResourceNotFoundException();
        }
        Podcast podcast = null;
        if(ctx.pathParamMap().containsKey("id")) {
            Long podcastId = ctx.pathParamAsClass("id", Long.class).get();
            podcast = repository.findOne(podcastId);
        }
        if (podcast == null) {
            if (failNotFound) {
                throw new ResourceNotFoundException();
            }
            else {
                podcast = new Podcast();
            }
        }
        return podcast;
    }

    Episode createOrGetEpisode(Context ctx, boolean failNotFound) throws ResourceNotFoundException {
        if(!ctx.pathParamMap().containsKey("episodeId") && failNotFound) {
            throw new ResourceNotFoundException();
        }
        Episode episode = null;
        if (ctx.pathParamMap().containsKey("episodeId")) {
            Long episodeId = ctx.pathParamAsClass("episodeId",Long.class).get();
            episode = repoEpisode.findOne(Long.valueOf(episodeId));
        }
        if (episode == null) {
            if (failNotFound) {
                throw new ResourceNotFoundException();
            }
            else {
                episode = new Episode();
            }
        }
        return episode;
    }
}
