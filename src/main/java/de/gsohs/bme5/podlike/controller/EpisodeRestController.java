package de.gsohs.bme5.podlike.controller;

import de.gsohs.bme5.podlike.dao.IRepository;
import de.gsohs.bme5.podlike.exception.ConversionException;
import de.gsohs.bme5.podlike.model.Episode;
import de.gsohs.bme5.podlike.model.RestEntityDto;
import de.gsohs.bme5.podlike.model.RestListDto;
import de.gsohs.bme5.podlike.util.EpisodeConverter;
import io.javalin.apibuilder.CrudHandler;
import io.javalin.http.Context;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller that handles RESTful HTTP calls (with JSON representation type). It implements
 * Javalin's {@link CrudHandler} interface to be compliant with common Create-Read-Update-Delete paradigm.
 * it uses a {@link de.gsohs.bme5.podlike.util.EpisodeConverter} in order to convert JPA-Entity objects to Data-Transfer-Objects
 */
public class EpisodeRestController implements IController, CrudHandler {

    private IRepository<Episode,Long> episodesRepository;
    private EpisodeConverter converter;

    /**
     * Constructor.
     * @param episodesRepository a {@link de.gsohs.bme5.podlike.dao.EpisodesRepository} instance
     * @param converter a {@link EpisodeConverter} instance
     */
    public EpisodeRestController(IRepository<Episode,Long> episodesRepository, EpisodeConverter converter) {
        this.episodesRepository = episodesRepository;
        this.converter = converter;
    }

    /**
     * creates a new Episode Resource via HTTP POST to the path '/api/podcasts/:podacstId/episodes'.
     * Successful creation is confirmed by HTTP Status 201 (created) and Location Header.
     *
     * @param ctx a {@link Context} instance to read request information and give back a response
     */
    @Override
    public void create(@NotNull Context ctx) {
        RestEntityDto body = ctx.bodyAsClass(RestEntityDto.class);
        Episode epi = null;
        try {
            epi = this.converter.reconvert(body, null);
            epi = this.episodesRepository.save(epi);
            ctx.header("Location","/api/podcasts/"+epi.getPodcast().getId()+"/episodes/"+epi.getId()).status(201);
        } catch (ConversionException e) {
            ctx.status(501);
        }
    }

    /**
     * deletes an episode Resource via HTTP DELETE to the path '/api/podcasts/:podacstId/episodes/:id'
     *
     * @param context a {@link Context} instance to read request information and give back a response
     * @param s the path param value for the resource identifier
     */
    @Override
    public void delete(@NotNull Context context, @NotNull String s) {
        this.episodesRepository.delete(this.episodesRepository.findOne(Long.parseLong(s)));
    }

    /**
     * retrieves a list of episodes via HTTP GET to the path '/api/podcasts/:podacstId/episodes'
     *
     * @param ctx a {@link Context} instance to read request information and give back a response
     */
    @Override
    public void getAll(@NotNull Context ctx) {
        List<RestEntityDto> resL = new ArrayList<>();
        for(Episode e : this.episodesRepository.findAll()) {
            resL.add(this.converter.convert(e));
        }
        RestListDto res = new RestListDto();
        res.setEmbed(resL);
        ctx.json(res);
    }

    /**
     * retrieves a episode Resource via HTTP GET to the path '/api/podcasts/:podacstId/episodes/:id'
     *
     * @param ctx a {@link Context} instance to read request information and give back a response
     * @param s the path param value for the resource identifier
     */
    @Override
    public void getOne(@NotNull Context ctx, @NotNull String s) {
        Episode e = this.episodesRepository.findOne(Long.parseLong(s));
        if(e == null) {
            ctx.status(404);
        } else {
            if(!e.getPodcast().getId().toString().equals(ctx.pathParam("podcastId"))) {
                ctx.status(400);
            }
            ctx.json(this.converter.convert(e));
        }
    }

    /**
     * updates n episode Resource via HTTP PATCH to the path '/api/podcasts/:podacstId/episodes/:id'
     *
     * @param ctx a {@link Context} instance to read request information and give back a response
     * @param s the path param value for the resource identifier
     */
    @Override
    public void update(@NotNull Context ctx, @NotNull String s) {
        RestEntityDto body = ctx.bodyAsClass(RestEntityDto.class);
        Episode epi = this.episodesRepository.findOne(Long.parseLong(s));
        try {
            epi = this.converter.reconvert(body, epi);
            epi.setId(Long.parseLong(s));
            epi = this.episodesRepository.save(epi);
            ctx.json(epi);
        } catch (ConversionException e) {
            ctx.status(501);
        }
    }
}
