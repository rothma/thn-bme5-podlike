package de.gsohs.bme5.podlike.controller;

import de.gsohs.bme5.podlike.dao.IRepository;
import de.gsohs.bme5.podlike.model.Episode;
import de.gsohs.bme5.podlike.model.Podcast;
import io.javalin.http.Handler;

import java.io.ByteArrayInputStream;

/**
 * a Controller that takes care of delivering binary contents
 * like the actual media of a podcast episode or images retrieved from database
 */
public class MediaController implements IController {

    private IRepository<Podcast,Long> repository;
    private IRepository<Episode,Long> episodeRepository;

    /**
     * Constructor of MediaController
     * @param repository a {@link de.gsohs.bme5.podlike.dao.PodcastRepository} instance
     * @param episodeRepository a {@link de.gsohs.bme5.podlike.dao.EpisodesRepository} instance
     */
    public MediaController(IRepository<Podcast,Long> repository, IRepository<Episode,Long> episodeRepository) {
        this.repository = repository;
        this.episodeRepository = episodeRepository;
    }

    /**
     * handles a HTTP get for the podcast title image
     */
    public Handler getImage = ctx -> {
        Podcast podcast = repository.findOne(Long.parseLong(ctx.pathParam("id")));
        byte[] img = podcast.getImage().getBinaryStream().readAllBytes();
        ctx.contentType("image/jpeg");
        ctx.result(img);
    };

    /**
     * handles a HTTP get for the podcast audio file
     */
    public Handler getAudio = ctx -> {
        Episode episode = episodeRepository.findOne(Long.parseLong(ctx.pathParam("episodeId")));
        byte[] audio = episode.getMedia().getBinaryStream().readAllBytes();
        ctx.contentType(episode.getMimeType());
        ctx.seekableStream(new ByteArrayInputStream(audio),"audio/mpeg");
    };
}
