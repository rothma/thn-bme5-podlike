package de.gsohs.bme5.podlike.controller;

/**
 * Common interface for all Application Controllers.
 * At the moment it acts solely as marker interface because no methods
 * are defined as contract to all Controllers
 */
public interface IController {
}
