package de.gsohs.bme5.podlike;

import de.gsohs.bme5.podlike.controller.IController;
import de.gsohs.bme5.podlike.dao.IRepository;

/**
 * defines factory methods in order to decouple Instance creation and actual implementation flavours.
 *
 * @see <a href="https://en.wikipedia.org/wiki/Factory_method_pattern">Factory Method Pattern</a>
 */
public interface IFactory {
    /**
     * factory method that creates a Repository by name
     * @param name the name of the desired repository
     * @return an IRepository implementation that is compliant with the given name or null if name is invalid.
     */
    IRepository createRepository(String name);

    /**
     * factory method that creates a Controller by name. It assumes that Controllers
     * are associated with one or more {@link IRepository} instances.
     * @param name the name of the desired controller
     * @param repositories one or more {@link IRepository} instances the controller collaborates with
     * @return an IController implementation that is compliant with the given name or null if name is invalid.
     */
    IController createController(String name,IRepository... repositories);
}
