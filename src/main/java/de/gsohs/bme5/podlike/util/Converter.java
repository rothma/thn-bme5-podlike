package de.gsohs.bme5.podlike.util;

import de.gsohs.bme5.podlike.exception.ConversionException;

/**
 * Generic interface that defines a contract of an object converter that is responsible
 * for converting different Data-Transfer-Object representations.
 *
 * @param <S> source type for conversion
 * @param <T> target type for conversion
 */
public interface Converter<S,T> {
    /**
     * converts an object representation from source to target type
     * @param source the source type instance to be converted
     * @return the converted object type
     */
    T convert(S source);

    /**
     * converts an object representation from target to source type.
     * the second param can be used to merge a source and target representation to
     * a merged source type
     * @param target the target type instance to be converted
     * @param Source the source type instance that can be merged with target type. may be null, then a new instance of that type will be created
     * @return the source type representation with merged information
     * @throws ConversionException if conversion runs into an error
     */
    S reconvert(T target, S Source) throws ConversionException;
}
