package de.gsohs.bme5.podlike.util;

import de.gsohs.bme5.podlike.exception.ConversionException;
import de.gsohs.bme5.podlike.model.Podcast;
import de.gsohs.bme5.podlike.model.RestEntityDto;

import java.util.Map;

/**
 * Converter implementation that converts a {@link Podcast} to a generic {@link RestEntityDto} that
 * can act as a JSON representation of a Podcast JPA-Entity and vice versa.
 */
public class PodcastConverter implements Converter<Podcast, RestEntityDto> {
    @Override
    public RestEntityDto convert(Podcast source) {
        RestEntityDto res = new RestEntityDto();
        res.setType("podcast");
        res.setId(source.getId());
        res.getAttributes().put("title",source.getTitle());
        res.getAttributes().put("author",source.getAuthor());
        res.getAttributes().put("description",source.getDescription());
        res.getAttributes().put("link",source.getLink());
        res.getAttributes().put("explicit",source.getExplicit().toString());
        RestEntityDto.Relationship selfRel = new RestEntityDto.Relationship();
        selfRel.setRel("self");
        selfRel.setHref("/api/podcasts/"+source.getId().toString());
        res.getLinks().add(selfRel);
        RestEntityDto.Relationship relEpi = new RestEntityDto.Relationship();
        relEpi.setHref("/api/podcasts/"+source.getId().toString()+"/episodes");
        relEpi.setRel("episodes");
        res.getRelationships().add(relEpi);
        RestEntityDto.Relationship relPic = new RestEntityDto.Relationship();
        relPic.setHref("/api/podcasts/"+source.getId().toString()+"/image");
        relPic.setRel("image");
        res.getRelationships().add(relPic);
        return res;
    }

    @Override
    public Podcast reconvert(RestEntityDto target, Podcast ret) throws ConversionException {
        if(ret == null) {
            ret = new Podcast();
        }
        if(target.getType() == null || target.getType().trim().equals("") || !target.getType().equals("podcast")) {
            throw new ConversionException();
        }
        if(target.getId() != null) {
            ret.setId(target.getId());
        }
        for(Map.Entry<String,Object> me : target.getAttributes().entrySet()) {
            switch (me.getKey()) {
                case "title" : ret.setTitle(me.getValue().toString()); break;
                case "author" : ret.setAuthor(me.getValue().toString()); break;
                case "description" : ret.setDescription(me.getValue().toString()); break;
                case "link" : ret.setLink(me.getValue().toString()); break;
                case "explicit" : ret.setExplicit(Boolean.valueOf(me.getValue().toString())); break;
            }
        }
        return ret;
    }
}
