package de.gsohs.bme5.podlike.util;

import de.gsohs.bme5.podlike.exception.ConversionException;
import de.gsohs.bme5.podlike.model.Episode;
import de.gsohs.bme5.podlike.model.RestEntityDto;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Converter implementation that converts a {@link Episode} to a generic {@link RestEntityDto} that
 * can act as a JSON representation of a Podcast JPA-Entity and vice versa.
 */
public class EpisodeConverter implements Converter<Episode, RestEntityDto>{
    @Override
    public RestEntityDto convert(Episode episode) {
        RestEntityDto toRet = new RestEntityDto();
        toRet.setType("episode");
        toRet.setId(episode.getId());
        toRet.getAttributes().put("title",episode.getTitle());
        if(episode.getPublicationDate() != null) {
            toRet.getAttributes().put("publicationDate", SimpleDateFormat.getDateInstance(DateFormat.SHORT).format(episode.getPublicationDate()));
        }
        toRet.getAttributes().put("description",episode.getDescription());
        toRet.getAttributes().put("mimeType",episode.getMimeType());
        RestEntityDto.Relationship selfRel = new RestEntityDto.Relationship();
        selfRel.setRel("self");
        selfRel.setHref("/api/podcasts/"+episode.getPodcast().getId().toString()+"/episodes/"+episode.getId().toString());
        toRet.getLinks().add(selfRel);
        RestEntityDto.Relationship relPod = new RestEntityDto.Relationship();
        relPod.setHref("/api/podcasts/"+episode.getPodcast().getId().toString());
        relPod.setRel("podcast");
        toRet.getRelationships().add(relPod);
        RestEntityDto.Relationship relMedia = new RestEntityDto.Relationship();
        relMedia.setHref("/api/podcasts/"+episode.getPodcast().getId().toString()+"/episodes/"+episode.getId().toString()+"/media");
        relMedia.setRel("media");
        toRet.getRelationships().add(relMedia);
        return toRet;
    }

    @Override
    public Episode reconvert(RestEntityDto target, Episode ret) throws ConversionException {
        if(ret == null) {
            ret = new Episode();
        }
        if(target.getType() == null || target.getType().trim().equals("") || !target.getType().equals("episode")) {
            throw new ConversionException();
        }
        if(target.getId() != null) {
            ret.setId(target.getId());
        }
        for(Map.Entry<String,Object> me : target.getAttributes().entrySet()) {
            switch (me.getKey()) {
                case "title" : ret.setTitle(me.getValue().toString()); break;
                case "description" : ret.setDescription(me.getValue().toString()); break;
                case "publicationDate" :
                    try {
                        ret.setPublicationDate(SimpleDateFormat.getDateInstance(DateFormat.SHORT).parse(me.getValue().toString()));
                    } catch (ParseException e) {
                        throw new ConversionException();
                    }
                    break;
                case "mimeType" : ret.setMimeType(me.getValue().toString()); break;
            }
        }
        return null;
    }
}
