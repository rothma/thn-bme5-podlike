package de.gsohs.bme5.podlike.controller;

import de.gsohs.bme5.podlike.dao.EpisodesRepository;
import de.gsohs.bme5.podlike.dao.IRepository;
import de.gsohs.bme5.podlike.dao.PodcastRepository;
import de.gsohs.bme5.podlike.exception.ResourceNotFoundException;
import de.gsohs.bme5.podlike.model.Episode;
import de.gsohs.bme5.podlike.model.Podcast;
import io.javalin.core.validation.Validator;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TestPodcastController {

    private PodcastController toTest;
    private IRepository<Podcast,Long> mockPodcastRepo;
    private IRepository<Episode,Long> mockEpisodeRepo;
    private Context mockCtx;

    @Test
    public void testGetPodcasts() {
        mockPodcastRepo = mock(PodcastRepository.class);
        mockEpisodeRepo = mock(EpisodesRepository.class);
        mockCtx = mock(Context.class);
        toTest = new PodcastController(mockPodcastRepo,mockEpisodeRepo);
        when(mockPodcastRepo.findAll()).thenReturn(Collections.singletonList(new Podcast()));
        final Handler getAllPodcasts = toTest.getAllPodcasts;
        try {
            getAllPodcasts.handle(mockCtx);
        } catch (Exception e) {
            fail(e.getMessage());
            e.printStackTrace();
        }
        verify(mockPodcastRepo,atLeastOnce()).findAll();
        verify(mockCtx).render(eq("get_podcasts.html"),anyMap());
    }

    @Test
    public void testCreateOrGetPodcast1() {
        mockPodcastRepo = mock(PodcastRepository.class);
        mockEpisodeRepo = mock(EpisodesRepository.class);
        mockCtx = mock(Context.class);
        toTest = new PodcastController(mockPodcastRepo,mockEpisodeRepo);
        when(mockCtx.pathParamMap()).thenReturn(Map.of("id","1"));
        when(mockCtx.pathParamAsClass("id",Long.class)).thenReturn(new Validator<Long>("1", Long.class, "id"));
        Podcast podToRet = new Podcast();
        when(mockPodcastRepo.findOne(Long.valueOf("1"))).thenReturn(podToRet);
        Podcast retO = null;
        try {
            retO = toTest.createOrGetPodcast(mockCtx,false);
        } catch (ResourceNotFoundException e) {
            fail();
            e.printStackTrace();
        }
        verify(mockCtx,atLeastOnce()).pathParamMap();
        verify(mockCtx,atLeastOnce()).pathParamAsClass(eq("id"),eq(Long.class));
        verify(mockPodcastRepo,atLeastOnce()).findOne(eq(1L));
        assertNotNull(retO);
        assertSame(podToRet,retO);
    }

    @Test
    public void testCreateOrGetPodcast2() {
        mockPodcastRepo = mock(PodcastRepository.class);
        mockEpisodeRepo = mock(EpisodesRepository.class);
        mockCtx = mock(Context.class);
        toTest = new PodcastController(mockPodcastRepo,mockEpisodeRepo);
        when(mockCtx.pathParamMap()).thenReturn(Map.of("somecrap","1"));
        Podcast retO = null;
        try {
            retO = toTest.createOrGetPodcast(mockCtx,false);
        } catch (ResourceNotFoundException e) {
            fail();
            e.printStackTrace();
        }
        assertNotNull(retO);
    }

    @Test
    public void testCreateOrGetPodcast3() {
        mockPodcastRepo = mock(PodcastRepository.class);
        mockEpisodeRepo = mock(EpisodesRepository.class);
        mockCtx = mock(Context.class);
        toTest = new PodcastController(mockPodcastRepo,mockEpisodeRepo);
        when(mockCtx.pathParamMap()).thenReturn(Map.of("somecrap","1"));
        assertThrows(
                ResourceNotFoundException.class,
                () -> toTest.createOrGetPodcast(mockCtx,true));
    }
}
