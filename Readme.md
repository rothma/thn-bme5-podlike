Beispielanwendung podlike
=======================
Dieses Repository beinhaltet den Quelltext für die Übung aus der Lehrveranstaltung
Internetprogrammierung an der Technischen Hochschule Nürnberg.

Die Webanwendung stellt ein Verzeichnis dar, das Audio-Podcasts listet und deren Episoden anzeigt
und auch abspielen lässt. Die Daten sind dabei in einer Postgres Datenbank gespeichert.
Berechtigte Benutzer können nach erfolgreichem Login Podcasts und Episoden anlegen und
bearbeiten

Dependencies
---------------
Die Anwendung setzt für das Kompilieren und Ausführen das Vorhandensein
eines Java SDK's (>= Version 17) voraus. Ich empfehle die Installation des
[Adoptium OpenJDK](https://adoptium.net/), da diese Downloads für alle gängigen
Betriebssysteme gepflegt und mit Installroutinen ausgeliefert werden.


Die Anwendung benutzt folgende Bibliotheken (Frameworks):
* [Javalin](https://javalin.io) als Web-Microframework mit embedded Jetty Servlet-Container
  und einfacher API für serverseitige Programmierung von HTTP und Websockets
* [Thymeleaf](https://www.thymeleaf.org) als Templating-Engine zur Erstellung dynamischer
  serverseitiger HTML-Ausgaben
* [Postgres-JDBC-Treiber](https://jdbc.postgresql.org) für die Konnektivität zu
  Postgres Instanzen in Java-Anwendungen via JDBC.
* [Hibernate ORM](https://hibernate.org/orm/) ein bekannter Open-Source Objekt-Relationale Mapping
  Engine

Datenbank
---------
Voraussetzung für den Betrieb ist eine installierte Postgres instanz, die mit einer
eigenen Datenbank und einem Benutzer darin mit ReadWrite Berechtigungen vorbereitet
worden ist. Bei einer frisch installierten Postgres-Datenbank kann via Postgres Client-Shell
eine Verbindung aufgebaut werden:

```
psql
```

Damit ist man mit der Masterdatenbank "postgres" verbunden und die weiteren Aktionen
finden per SQL DDL Statements statt. Zuerst wird eine eigene Datenbank 'podlike' angelegt':

`create database podlike;`

Danach ein Datenbankbenutzer mit dazugehörigem Passwort:

`create user podlike password 'podlike';`

Nun wird die neue Datenbank dem Benutzer 'podlike' als Eigentümer zugewiesen. Damit
hat dieser User umfangreiche Berechtigungen auf dieser Datenbank.

`alter database podlike owner to podlike;`

Damit auch aus dem Netzerk eine Verbindung aufgebaut werden kann, muss dem User 'podlike'
 explizit erlaubt werden, dies für die Datenbank 'podlike' zu tun

grant connect on database podlike to podlike;`

Zuletzt wird innerhalb der Datenbank 'podlike' ein Schema mit gleichem Namen angelegt
und auch dem richtigen Beutzer als Eigentümer zugewiesen:

```
create schema 'podlike';
alter schema podlike owner to podlike;
```

Nun kann die Postgres Client Shell per '\q' verlassen werden.


Bauen und Ausführung der Anwendung
-------------------
Da dieses Projekt [maven](https://maven.apache.org) verwendet, ist eine Installation von
Apache maven (standalone oder in der IDE integriert) notwendig. im Wurzelverzeichnisses
dieses Projektes genügt zum Bauen folgender Befehl

```
mvn package
```
Dadurch wird die Anwendung als Self-Contained-App gebaut und im
generierten Verzeichnis 'target' abgelegt.

Das Ausführen kann in der Kommandozeile erfolgen durch:
```
cd target
java -jar podlike-full.jar
```

Aufbau und Design
-----------------
Die Anwendung ist in folgende Packages untergliedert:
* de.gsohs.bme5.podlike.Main:  MainClass und Factory und Builders (vgl. Creational Patterns)
  ![UML Diagramm App-Classes](src/site/img/UML-App-podlike-Builders.png)
* de.gsohs.bme5.podlike.controller: Controller, die HTTP-Endpoint-Handler ausgestalten und
  die Business-Logik implementieren.
* de.gsohs.bme5.podlike.dao: Repositories, die unter Verwendung einer Datenbankverbindung
  die Daten aus ihr lesen und manipulieren und die Ergebnisse an die aufrufenden Controller
  weiterreichen
  ![UML Diagramm App-Classes](src/site/img/UML-App-podlike.png)
* de.gsohs.bme5.podlike.model: JPA Entities, die die Datenbank-Dokumente als Java-Objekte
  abbilden und in den Repositories und Controllers als Daten-Transfer-Objekte benutzt werden

Dokumentation
-------------
Alle Klassen und auch die pom.xml sind kommentiert, so dass an den betreffenden Codestellen
Funktionsweise und weiterführende Kontextinformation dort entnommen werden können.